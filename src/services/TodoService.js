import axios from "axios"

const TODO_API_BASE_URL = 'http://localhost:8080/api/todos'

class TodoService {
    getTodos() {
        return axios.get(TODO_API_BASE_URL);
    }

    addTodo(description) {
        return axios.post(TODO_API_BASE_URL, {
            description: description,
            done: false
        });
    }

    editTodoDescription(id, description) {
        return axios.post(TODO_API_BASE_URL + '/' + id, {
            description: description
        });
    }

    editTodoDone(id, newValue) {
        return axios.patch(TODO_API_BASE_URL + '/' + id, {
            done: newValue
        });
    }

    deleteTodo(id) {
        return axios.delete(TODO_API_BASE_URL + '/' + id);
    }
}

export default new TodoService()